<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/category/categories/{category}', 'CategoryController@showDescendants')->name('categories');
Route::get('/product/{category_id}', 'ProductController@index')->name('category_products');
Route::get('/product/show/{product}', 'ProductController@show')->name('product_card');
Route::get('/role/attach', 'RoleController@attachRole');
Route::get('/role/detach/{?id}', 'RoleController@detachRole');
Route::get('/role/detach/{?id}', 'RoleController@detachRole');
Route::get('/order/create/{product}', 'OrderController@create')->name('order_create');
Route::get('/order/cart', 'OrderController@showCart')->name('cart');
Route::match(['get', 'post'],'/order/update/{product}', 'OrderController@updateQuantity');
Route::match(['get', 'post'],'/order/checkout/step2', 'OrderController@editStep2');
Route::match(['get', 'post'],'/order/checkout/add', 'OrderController@updateCheckout');
Route::match(['get', 'post'],'/order/checkout/step1', 'OrderController@editStep1');
Route::match(['get', 'post'],'/order/delete/{product_id}', 'OrderController@deleteProduct');
Route::match(['get', 'post'],'/order/test', 'OrderController@test');

Route::middleware(['admin','auth'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', 'Admin\AdminDashboardController@index')->name('admin');
        Route::get('/product', 'Admin\AdminProductController@index')->name('admin_product');
        Route::get('/category', 'Admin\AdminCategoryController@index')->name('admin_category');
        Route::match(['get', 'post'],'/category/forms-for-create', 'Admin\AdminCategoryController@editFormsForCreate')
            ->name('forms_for_create_category');
        Route::match(['get', 'post'],'/category/create', 'Admin\AdminCategoryController@create')
            ->name('create_category');
        Route::match(['get', 'post'],'/category/forms-for-delete', 'Admin\AdminCategoryController@editFormsForDelete')
            ->name('forms_for_delete_category');
        Route::match(['get', 'post'],'/category/delete/', 'Admin\AdminCategoryController@delete')
            ->name('delete_category');
        Route::match(['get', 'post'],'/category/forms-for-update', 'Admin\AdminCategoryController@editFormsForUpdate')
            ->name('forms_for_update_category');
        Route::match(['get', 'post'],'/category/update', 'Admin\AdminCategoryController@update')
            ->name('update_category');
        Route::match(['get', 'post'],'/product/create', 'Admin\AdminProductController@create')
            ->name('create_product');
        Route::match(['get', 'post'],'/product/forms-for-create', 'Admin\AdminProductController@editFormsForCreate')
            ->name('forms_for_create_product');
        Route::match(['get', 'post'],'/product/delete', 'Admin\AdminProductController@delete')
            ->name('delete_product');
        Route::match(['get', 'post'],'/product/forms-for-delete', 'Admin\AdminProductController@editListForDelete')
            ->name('list_for_delete_product');
        Route::match(['get', 'post'],'/product/update', 'Admin\AdminProductController@update')
            ->name('update_product');
        Route::match(['get', 'post'],'/product/list-for-update', 'Admin\AdminProductController@editListForUpdate')
            ->name('list_for_update_product');
        Route::match(['get', 'post'],'/product/forms-for-update', 'Admin\AdminProductController@formsForUpdate')
             ->name('forms_for_update_product');
        Route::get('/role', 'Admin\AdminRoleController@index')->name('admin_role');
    });
});

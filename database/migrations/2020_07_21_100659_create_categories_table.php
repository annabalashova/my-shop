<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->nestedSet();
            $table->id();
            $table->string('name', 255);
            $table->string('image', 255);
            $table->timestamps();
        });
    }

    /**p
     * Reverse the migrations.
     *
     * @return voidcomposer update
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropNestedSet();
            Schema::dropIfExists('categories');
        });
    }
}

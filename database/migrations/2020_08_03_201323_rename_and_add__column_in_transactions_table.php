<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameAndAddColumnInTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->bigInteger('order_id', false, true)->after('name');
            $table->date('date')->nullable()->after('order_id');
            $table->renameColumn('name', 'type');

            $table->foreign('order_id')->on('orders')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->renameColumn('type', 'name');
            $table->dropForeign('transactions_order_id_foreign');
            $table->dropColumn('order_id');
            $table->dropColumn('date');
        });
    }
}

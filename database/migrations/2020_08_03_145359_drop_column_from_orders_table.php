<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnFromOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_payment_id_foreign');
            $table->dropForeign('orders_delivery_id_foreign');
            $table->dropColumn('payment_id');
            $table->dropColumn('delivery_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('payment_id', false, true)->nullable();
            $table->bigInteger('delivery_id', false, true)->nullable();
            $table->foreign('payment_id')->on('payments')->references('id');
            $table->foreign('delivery_id')->on('deliveries')->references('id');
        });
    }
}

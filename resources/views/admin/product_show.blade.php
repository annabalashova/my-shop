@extends('admin.admin_main')

@section('content')
    <div>
        <h2>Категория</h2>
        <p>Наименование: {{ $category->name }}</p>
        <p>Родительские категории:</p>
        @foreach($category->ancestors as $ancestor)
            <li><a href="{{ route('category.show', ['category' => $ancestor]) }}">{{ $ancestor->name }}</a></li>
        @endforeach
        <p>Дочерние категории:</p>
        <table>
            <tr>
                <th>Наименование</th>
                <th>Количество товара</th>
                <th>Редактировать категорию</th>
                <th>Удалить категорию</th>
            </tr>
            <tr>
                @foreach($category->descendants as $descendant)
                    <td>
                        <a href="{{ route('category.show', ['category' => $descendant]) }}">{{ $descendant->name }}</a>
                    </td>
                    <td>{{ $descendant->products->count() }}</td>
                    <td>
                        <a href="{{ route('category.edit', ['category' => $descendant]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form method="post" action="{{ route('category.destroy', ['category' => $descendant]) }}">
                            @method('DELETE')
                            {{ csrf_field()}}
                            <button type="submit" name="uploade">Удалить</button>
                        </form>
                    </td>
            </tr>
            @endforeach
        </table>
        <br>
        <a href="{{ route('category.edit', ['category' => $category]) }}">Редактировать</a>
        <br><br>
        <form method="post" action="{{ route('category.destroy', ['category' => $category]) }}">
            @method('DELETE')
            {{ csrf_field()}}
            <button type="submit" name="uploade">Удалить</button>
        </form>
    </div>
@endsection

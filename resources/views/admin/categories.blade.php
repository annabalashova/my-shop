@extends('admin.admin_main')

@section('content')
    <div>
        <h2>Роли и разрешения</h2>
        <p><a href="{{ route('role.create') }}">Создать роль</a></p>
        <table cellpadding="5">
            <tr>
                <th>Наименованиe</th>
                <th>Редактировать роль</th>
                <th>Удалить роль</th>
            </tr>
            @foreach($roles as $role)
                <tr>
                    <td>
                        <a href="{{ route('role.show', ['role' => $role]) }}">{{ $role->name }} </a>
                    </td>
                    <td>
                        <a href="{{ route('role.edit', ['role' => $role]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form method="post" action="{{ route('role.destroy', ['role' => $role]) }}">
                            @method('DELETE')
                            {{ csrf_field()}}
                            <button type="submit" name="uploade">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection

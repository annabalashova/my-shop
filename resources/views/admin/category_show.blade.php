@extends('admin.admin_main')

@section('content')
    <div>
        <h2>Роль</h2>
        <p>Наименование: {{ $role->name }}</p>
        <p>Описание: {{ $role->description }}</p>
        <p>Закрепленные разрешения:</p>
        <table>
            @foreach($role->permissions as $permission)
                @if($loop->index == 0 || $loop->index % 3 == 0)
                    <tr>
                        <td> {{ $permission->name }}</td>
                        @elseif( $loop->index % 3 == 1)
                            <td> {{ $permission->name }}</td>
                        @elseif( $loop->index % 3 == 2)
                            <td> {{ $permission->name }}</td>
                    </tr>
                @endif
            @endforeach
        </table>
        <br>
        <a href="{{ route('role.edit', ['role' => $role]) }}">Редактировать</a>
        <form method="post" action="{{ route('role.destroy', ['role' => $role]) }}">
            <br><br>
            @method('DELETE')
            {{ csrf_field()}}
            <button type="submit" name="uploade">Удалить</button>
        </form>
    </div>
@endsection

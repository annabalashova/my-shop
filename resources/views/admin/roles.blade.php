@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Роли и разрешения</h3>
        <table border cellspacing="0">
                <tr>
                    <th>Наименованиe</th>
                    <th>Pазрешения</th>
                    <th>Удалить роль</th>
                    <th>Редактировать  роль</th>
                    <th>Назначить роль пользователю</th>
                </tr>
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $role->name }}</td>
                        <td>
                            @foreach($role->permissions as $permission)
                                {{ $permission->name }}<br>
                       @endforeach
                        </td>
                        <td>
                            <a href="{{ route('role_delete') }}">Удалить роль</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

@endsection

@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Создание категории</h3>
        <form method="post" action="{{ route('create_category') }}" autocomplete="on">
            {{ csrf_field() }}
            <label>Категория</label>
            <input name="name">
            <br><br>
            <label>Родительская категория</label>
            <br>
            <select name="parent_id">
                @foreach($categories as $id => $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <br><br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>
@endsection

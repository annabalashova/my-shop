@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Выбрать товары заданной категории</h3>
        <ul>
            @foreach($category->children as $child)
                <li>
                    <a href="{{ route('product.category', ['category' => $child]) }}">{{ $child->name }}</a>
                </li>
            @endforeach
        </ul>
        <h3>Список товаров</h3>
        <table>
            <tr>
                <th>Наименование</th>
                <th>Категория</th>
                <th>Цена</th>
                <th>Редактировать товар</th>
                <th>Удалить товар</th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>
                        <a href="{{ route('product.show', ['product' => $product->id]) }}">{{ $product->name }}</a>
                    </td>
                    <td>
                        <a href="{{ route('category.show', ['category' => $product->category->id]) }}">
                            {{ $product->category->name }}</a>
                    </td>
                    <td>{{ $product->price }}</td>
                    <td>
                        <a href="{{ route('product.edit', ['product' => $product->id]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form method="post" action="{{  route('product.destroy', ['product' => $product->id]) }}"
                              autocomplete="on">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button type=submit name="uploade">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $products->render() }}
    </div>

@endsection

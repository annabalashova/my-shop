@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Введите email пользователя</h3>
        <form method="post" action="{{ route('role_show') }}" autocomplete="on">
            {{ csrf_field() }}
            <input name="email">
            <br><br>
            <button type="submit" name="upload">Подтвердить</button>
        </form>
    </div>
@endsection

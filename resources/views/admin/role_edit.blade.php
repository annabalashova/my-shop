@extends('admin.admin_main')

@section('content')
    <div>
        <h2>Редактировать роль</h2>
        <form method="post" action="{{ route('role.update', ['role' => $role->id]) }}">
        {{ csrf_field() }}
        @include('admin.includes.role_part')
        </form>
            </div>
@endsection

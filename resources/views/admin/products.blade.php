@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Удалить товары заданной категории</h3>
        <form method="post" action="{{ route('list_for_delete_product') }}"
              autocomplete="on">            {{ csrf_field() }}
            {{ csrf_field() }}
            <select name="category_id" onchange="this.form.submit()">
                <option value="0" selected>Выберите категорию</option>
                @foreach($categoriesTree as $id => $category )
                    <option value="{{ $id }}">{{ $category }}</option>
                @endforeach
            </select>
        </form>
        <h3>Список товаров</h3>
        <form method="post" action="{{ route('delete_product') }}" autocomplete="on">
            {{ csrf_field() }}
            <table border="0">
                <tr>
                    <th>Категория</th>
                    <th>Наименование</th>
                    <th>Цена</th>
                    <th>Удалить</th>
                </tr>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->category->ancestors()->count() ? implode(' -> ', $product->category->ancestors->where('name', '<>', 'Категории')->pluck('name')->toArray()) : 'Top Level' }}
                            ->{{ $product->category->name }} </td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            <input type="checkbox" name="id[]" value="{{ $product->id }}"
                                   @if($categoryId == $product->category_id) checked @endif>
                        </td>
                    </tr>
                @endforeach
            </table>
            <button type=submit name="uploade" value="delete">Удалить окончательно</button>
            <button type=submit name="uploade" value="update">Перевести в неактивные</button>
        </form>
        {{ $products->render() }}
    </div>

@endsection

@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Переименовние категории</h3>

        <form method="post" action="{{ route('update_category') }}" autocomplete="on">
            {{ csrf_field() }}
            <label>Новое название категории</label>
            <input name="new_name">
            <br><br>
            <select name="id">
                <option value="0" selected>Выберите категорию</option>
                @foreach($categoriesTree as $id => $category )
                    @if($id == 1)
                        @continue
                    @endif
                    <option value="{{ $id }}">{{ $category }}</option>
                @endforeach
            </select>
            <br><br>
            <button type="submit" name="upload">Переименовать</button>
        </form>
    </div>
@endsection

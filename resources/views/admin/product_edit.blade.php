@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Редактирование категории</h3>
        <form method="post" action="{{ route('category.update', ['category' => $category]) }}" autocomplete="on">
            {{ csrf_field() }}
            @method('PUT')
            @include('admin.includes.category_part')
        </form>
    </div>
@endsection

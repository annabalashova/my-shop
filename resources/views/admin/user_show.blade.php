@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Пользователь</h3>
        <p>Имя: {{ $user->name }}</p>
        <p>Email: {{ $user->email }}</p>
        <p>Закрепленные роли:</p>
        @foreach( $user->roles as $role )
            <table border cellspacing="0" cellpadding="5">
                <tr>
                    <td>
                        {{ $role->name }}
                    </td>
                    <td>
                        <a href="{{ route('user_detach_role', ['user' => $user, 'role' => $role] ) }}">Отвязать роль</a>
                    </td>
                </tr>
            </table>
        @endforeach
    </div>
@endsection

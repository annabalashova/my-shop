@if(isset($category) && $child->id == $category->id  && $child->children->count())
    @foreach($categories as $child)
        <li><a href="{{ route('admin.products', ['category' => $child]) }}">{{ '--'.$child->name }}</a></li>
        @include('admin.includes.product_part_category', ['categories' => $child])
    @endforeach
@endif

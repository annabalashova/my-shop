    <label>Наименование</label>
    <br>
    <input name="name" value="{{ old('name') ?? $role->name ?? ''}}">
    <br><br>
    <label>Описание</label>
    <br>
    <textarea name="description" cols="36" rows="3">
                {{ old('description') ?? $role->description  ?? ''}}</textarea>
    <br><br>
    <label>Выбрать разрешение</label>
    <br><br>
    <table>
        @foreach($permissions as $permission)
            @if($loop->index == 0 || $loop->index % 3 == 0)
                <tr>
                    <td> {{ $permission->name }}</td>
                    <td>
                        <input type="checkbox" name="permission_id[]" value="{{ $permission->id }}"
                               @if(in_array($permission->id, $rolePermissions)) checked @endif >
                    </td>
                    @elseif( $loop->index % 3 == 1)
                        <td> {{ $permission->name }}</td>
                        <td>
                            <input type="checkbox" name="permission_id[]" value="{{ $permission->id }}"
                                   @if(in_array($permission->id, $rolePermissions)) checked @endif>
                        </td>
                    @elseif( $loop->index % 3 == 2)
                        <td> {{ $permission->name }}</td>
                        <td>
                            <input type="checkbox" name="permission_id[]" value="{{ $permission->id }}"
                                   @if(in_array($permission->id, $rolePermissions)) checked @endif>
                        </td>
                </tr>
            @endif
        @endforeach
    </table>
    <br>
    <button type="submit" name="upload">Сохранить</button>


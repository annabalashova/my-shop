<label>Название категории</label>
<input name="name" value="{{ old('name') ?? $category->name  ?? '' }}">
<br><br>
<label>Родительская категория</label>
<select name="parent_id">
    <option value="0" selected>Выберите категорию</option>
    @foreach($categoriesTree as $parent_id => $name )
        <option value="{{ $parent_id }}" @isset($category )@if($category->parent_id == $parent_id) selected @endif @endisset>{{ $name }}</option>
    @endforeach
</select>
<br><br>
<button type="submit" name="upload">Coхранить</button>

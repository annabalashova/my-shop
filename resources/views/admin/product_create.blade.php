@extends('admin.admin_main')

@section('content')
    <div>
        <h3>Создание категории</h3>
        <form method="post" action="{{ route('category.store') }}" autocomplete="on">
            {{ csrf_field() }}
            @include('admin.includes.category_part', ['category' => null])
        </form>
    </div>
@endsection

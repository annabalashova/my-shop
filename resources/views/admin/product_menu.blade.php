@extends('admin.admin_main')

@section('menu')
    <li><a href="{{ route('forms_for_create_product') }}">Добавить товар</a></li>
    <li><a href="{{ route('list_for_delete_product') }}">Удалить товар</a></li>
    <li><a href="{{ route('list_for_update_product') }}">Изменить товар</a></li>
@endsection

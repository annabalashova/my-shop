@extends('welcome')

@section('content')
    <div class="products">
        <div class="breadcrumbs">
            @foreach($ancestors as $ancestor)
                <a href="{{ route('categories', ['category' => $ancestor->id]) }}">{{ $ancestor->name }}</a> ->
            @endforeach
            <p>{{ $category->name }}</p>
        </div>
        <div class="product_show">
            @foreach($products as $product)
                <li>
                    <a href="{{ route('product_card', ['product' => $product->id]) }}">{{ $product->name }}</a>
                    @if(!empty($product->orders()->where([
            ['user_id', Auth::id()],
            ['status', 'new']
            ])->first())
            || in_array($product->id, $getCartProductsId))
                        в корзине
                    @else
                        <a href="{{ route('order_create',['product' => $product->id]) }}">В корзину</a>
                </li>
                @endif
            @endforeach
        </div>
    </div>
@endsection


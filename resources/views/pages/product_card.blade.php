@extends('welcome')

@section('content')
    <div class="product">
        <div class="breadcrumbs">
            @foreach($ancestors as $ancestor)
                <a href="{{ route('categories', ['category' => $ancestor->id]) }}">{{ $ancestor->name }}</a> ->
            @endforeach
            <a href="{{ route('category_products', ['category_id' => $product->category_id]) }}">{{ $product->category->name }}</a>
        </div>
        <div class="product_card">
            <p>{{ $product->image }}</p>
            <p>{{ $product->name }}</p>
            <p>Цена: {{ $product->price }} грн.</p>
            <p>Описание товара:</p>
            <p>{{ $product->description }}</p>
            @if(!empty($product->orders()->where([
           ['user_id', Auth::id()],
           ['status', 'new']
           ])->first())
           || in_array($product->id, $getCartProductsId))
                Товар находится в корзине
            @else
                <a href="{{route('order_create', ['product' => $product->id]) }}">В корзину</a>
            @endif
        </div>
    </div>
@endsection

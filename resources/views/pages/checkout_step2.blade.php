@extends('welcome')

@section('content')
    <div>
        <h3>Оформление заказа</h3>
        <form method="post" action="/order/checkout/add" autocomplete="on">
            {{ csrf_field() }}
            <lablel>Выберите способ доставки</lablel>
            <br>
            <p><input name="delivery" type="radio" value="courier">Курьерская доставка</p>
            <p><input name="delivery" type="radio" value="mail">Доставка почтой</p>
            <p><input name="delivery" type="radio" value="pickup">Самовывоз</p>
            <br>
            <lablel>Выберите способ оплаты</lablel>
            <br>
            <p><input name="payment" type="radio" value="cash">Наличными при получении</p>
            <p><input name="payment" type="radio" value="card">Оплата банковской картой</p>
            <p><input type="submit" value="Выбрать"></p>
        </form>
    </div>
@endsection

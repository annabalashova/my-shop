@extends('welcome')

@section('content')
    <div class="product_show">
        <table border="0">
            @if(!empty($products))
                <tr>
                    <th>Название товара</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Cумма</th>
                    <th></th>
                </tr>
                @php($sum = 0)
                @foreach($products as $items)
                    @foreach($items as $key => $product )
                        <tr>
                            <td>{{ $product['name'] }}</td>
                            <td>
                                <form method="post" action="/order/update/{{ $key }}" autocomplete="on" name="this">
                                    {{ csrf_field() }}
                                    <button type="submit" name="decrease" value="{{ $product['quantity']-1 }}">-
                                    </button>
                                    <input type="text" size="1" name="quantity" value="{{ $product['quantity'] }}"
                                           onchange="this.form.submit()">
                                    <button type="submit" name="increase" value="{{ $product['quantity']+1 }}">+
                                    </button>
                                </form>
                            </td>
                            <td>
                            <td>
                                {{ $product['price'] }}
                            </td>
                            <td>{{ $product['price'] * $product['quantity'] }}</td>
                            <td>
                                <a href="/order/delete/{{ $key }}">Удалить</a>
                            </td>
                        </tr>
                        @php($sum = $sum + $product['price'] * $product['quantity'])
                    @endforeach
                @endforeach
        </table>
        <p>Cумма заказа: {{ $sum }} грн.</p>
        <p>
            <a href="/category/categories/1">Продолжить покупки</a>
            <a href="/order/checkout/step1">Оформить заказ</a>
        </p>
    </div>
    @else
        <p>В корзине нет товаров</p>
    @endif
@endsection



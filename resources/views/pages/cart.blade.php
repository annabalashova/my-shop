@extends('welcome')

@section('content')
@if(!empty($order))
<div class="product_show">
    <table border="0">
        <tr>
            <th>Название товара</th>
            <th>Количество</th>
            <th>Цена</th>
            <th>Cумма</th>
            <th></th>
        </tr>
        @php($sum = 0)
        @foreach($order->products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>
                <form method="post" action="/order/update/{{ $product->id }}" autocomplete="on" name="this">
                    {{ csrf_field() }}
                    <button type="submit" name="decrease" value="{{ $product->pivot->quantity-1 }}">-
                    </button>
                    <input type="text" size="1" name="quantity" value="{{ $product->pivot->quantity }}"
                           onchange="this.form.submit()">
                    <button type="submit" name="increase" value="{{ $product->pivot->quantity+1 }}">+
                    </button>
                </form>
            </td>
            <td>
            <td>
                {{ $product->pivot->price }}
            </td>
            <td>{{ $product->pivot->price * $product->pivot->quantity }}</td>
            <td>
                <a href="/order/delete/{{ $product->id }}">Удалить</a>
            </td>
        </tr>
        @php($sum = $sum + $product->pivot->price * $product->pivot->quantity)
        @endforeach
    </table>
    <p>Cумма заказа: {{ $sum }} грн.</p>
    <p>
        <a href="/category/categories/1">Продолжить покупки</a>
        <a href="/order/checkout/step2">Оформить заказ</a>
    </p>
</div>
@else
<p>В корзине нет товаров</p>
@endif
@endsection


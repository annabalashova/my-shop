<?php


namespace App\Listeners;


use App\Services\OrderService;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class UpdateOrder
{
    public function handle()
    {
        $idSession = Cookie::get('oldId');
        $sessionProducts = session()->get($idSession);
        $order = app()->get(OrderService::class)->setOrder();
        $orderProducts = $order->products->pluck('id')->toArray();
        if ($idSession) {
            foreach ($sessionProducts as $item) {
                foreach ($item as $key => $product) {
                    if(in_array($key, $orderProducts)){
                        $order->products()->detach($key);
                    }
                    $order->products()->attach($key, ['price' => $product['price'], 'quantity' => $product['quantity']]);
                }
            }
        }
        Cookie::queue('oldId', null);
    }
}

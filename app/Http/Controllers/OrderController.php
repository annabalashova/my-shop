<?php


namespace App\Http\Controllers;


use App\Services\OrderService;
use App\Models\Category;
use App\Models\Delivery;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    protected function getOrder()
    {
        $order = Order::where([
            ['user_id', Auth::id()],
            ['status', 'new']
        ])->first();
        return $order;
    }

    public function create(Product $product, Request $request)
    {
        if (Auth::user()) {
            $order = $this->getOrder();
            if (empty($order)) {
                $order = new Order();
                $order->user_id = Auth::id();
                $order->date = Carbon::now();
                $order->status = 'new';
                $order->save();
            }

            $order->products()->attach($product->id, ['price' => $product->price, 'quantity' => 1]);
        } else {
            $this->putSessionData($product);
        }

        return redirect()->route('category_products', [$product->category_id]);
    }

    public function showCart(Request $request)
    {
        if (Auth::user()) {
            $order = $this->getOrder();
            foreach ($order->products as $product) {
                if ($product->price != $product->pivot->price) {
                    $order->products()->updateExistingPivot($product->id, ['price' => $product->price]);
                }
            }
            return view('pages.cart', ['order' => $order]);
        } else {
            $id = session()->getId();
            $products = session()->get($id);
            return view('pages.cart_no_auth', ['products' => $products]);
        }
    }

    public function updateQuantity(Request $request, Product $product)
    {
        if ($request->decrease) {
            $quantity = $request->decrease;
        } elseif ($request->increase) {
            $quantity = $request->increase;
        } else {
            $quantity = $request->quantity;
        }
        if (Auth::user()) {
            $order = $this->getOrder();
            $product->orders()->updateExistingPivot($order->id, ['quantity' => $quantity]);
        } else {
            $id = session()->getId();
            $products = session()->get($id);
            $newProducts = [];
            foreach ($products as $items) {
                if (array_key_exists($product->id, $items)) {
                    $items[$product->id]['quantity'] = $quantity;
                    $newProducts[] = $items;
                } else {
                    $newProducts[] = $items;
                }
            }
            session()->forget($id);
            session()->put($id, $newProducts);
        }
        return redirect()->route('cart');
    }

    public function editStep2()
    {
        return view('pages.checkout_step2');
    }

    public function editStep1()
    {
        return view('pages.checkout_step1');
    }

    public function updateCheckout(Request $request)
    {
        DB::transaction(function () use ($request) {
            $delivery = new Delivery;
            $delivery->type = $request->delivery;
            $delivery->order_id = $this->getOrder()->id;
            $delivery->save();

            $transaction = new Transaction();
            $transaction->type = $request->payment;
            $transaction->order_id = $this->getOrder()->id;
            $transaction->save();

            $order = $this->getOrder();
            $order->status = 'checkout';
            $order->save();
        });
        return redirect()->route('welcome')->with('message', "Ваш заказ успешно оформлен");
    }

    protected function putSessionData(Product $product)
    {
        $id = session()->getId();
        $products = [];
        $products[$product->id] = [
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => 1];
        session()->push($id, $products);
        Cookie::queue('oldId', $id);
    }

    public function deleteProduct($productId)
    {
        if (Auth::user()) {
            $order = $this->getOrder();
            $order->products()->detach($productId);
        } else {
            $idSession = session()->getId();
            $products = session()->get($idSession);
            foreach ($products as $index => $item) {
                foreach ($item as $key => $product)
                    if ($productId == $key) {
                        unset($products[$index]);
                        session()->forget($idSession);
                        session()->put($idSession, $products);
                    }
            }
        }
        return redirect()->route('cart');
    }

    public function test()
    {
        $orders = Order::where('status', 'new')->get();
        $date = Carbon::now()->subDays(10);
        foreach ($orders as $order)
            if ($order->updated_at > $date) {
                $order->products()->detach();
                $order->delete();
            }
    }
}

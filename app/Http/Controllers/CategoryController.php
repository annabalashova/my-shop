<?php


namespace App\Http\Controllers;


use App\Models\Category;

class CategoryController extends Controller
{
    public function showDescendants(Category $category)
    {
        $categoryDescendants = Category::defaultOrder()->descendantsOf($category->id);
        return view('pages.category_descendants', [
            'category' => $category,
            'categoryDescendants' => $categoryDescendants]);
    }

}

<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AdminProductController extends Controller
{
    public function index()
    {
        return view('admin.product_menu');
    }

    public function editFormsForCreate(CategoryService $service)
    {
        $categoriesTree = $service->getCategoriesTree();
        return view('admin.product_create_forms', ['categoriesTree' => $categoriesTree]);
    }

    public function create(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category_id;
        $product->description = $request->description;
        $product->image = $request->image;
        $product->status = 'active';
        if ($product->save()) {
            return redirect()->route('admin_product')->with('message', 'Товар успешно добавлен');
        }
    }

    public function editListForDelete(Request $request, CategoryService $service)
    {
        $categoryId = $request->category_id;
        $categoriesTree = $service->getCategoriesTree();
        $products = Product::where('status', 'active')->with('category')->orderBy('category_id')->paginate(15);
        return view('admin.product_delete_list', [
            'products' => $products,
            'categoriesTree' => $categoriesTree,
            'categoryId' => $categoryId
        ]);
    }

    public function delete(Request $request)
    {
        $productsId = $request->id;
        if ($request->uploade == 'delete') {
            foreach ($productsId as $productId) {
                Product::where('id', $productId)->delete();
            }
            return redirect()->route('admin_product')->with('message', 'Товар успешно удален');
        } elseif ($request->uploade == 'update') {
            foreach ($productsId as $productId) {
                Product::where('id', $productId)->update(['status' => 'nonactive']);
            }
            return redirect()->route('admin_product')->with('message', 'Товар переведен в неактивные');
        }
    }

    public function editListForUpdate(CategoryService $service)
    {
        $categoriesTree = $service->getCategoriesTree();
        $products = Product::where('status', 'active')->with('category')->orderBy('category_id')->paginate(15);
        return view('admin.product_update_list', [
            'products' => $products,
            'categoriesTree' => $categoriesTree,
        ]);
    }

    public function formsForUpdate(Request $request, CategoryService $service)
    {
        $categoriesTree = $service->getCategoriesTree();
        if (count($request->id) == 1) {
            $product = Product::where('id', $request->id)->first();
        } else {
            $product = null;
        }
        Cache::put('update_products', $request->id, 600);
        return view('admin.product_update_forms', [
            'product' => $product,
            'categoriesTree' => $categoriesTree,
        ]);
    }

    public function update(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $data = [];
        foreach ($input as $key => $attribute) {
            if ($attribute) {
                $data[$key] = $attribute;
            }
        }
        $array = Cache::get('update_products');
        Product::whereIn('id', $array)->update($data);

        return redirect()->route('admin_product')->with('message', 'Изменения успешно выполнены');
    }
}



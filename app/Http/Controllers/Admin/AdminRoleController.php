<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class AdminRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('permissions')->get();
        return response()->view('admin.roles', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $permissions = Permission::all();
        return response()->view('admin.role_create', ['permissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $role = new Role();
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        foreach ($request->permission_id as $permission_id)
            $role->permissions()->attach($permission_id);
        return redirect()->route('roles')->with('message', 'Новая роль успешно создана');
    }

    /**
     * Display the specified resource.
     *
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return response()->view('admin.role_show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all();
        $rolePermissions = $role->permissions->pluck('id')->toArray();
        return response()->view('admin.role_edit', [
            'role' => $role,
            'rolePermissions' => $rolePermissions,
            'permissions' => $permissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Role $role)
    {
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        $role->permissions()->detach();
        foreach ($request->permission_id as $id) {
            $role->permissions()->attach($id);
        }
        return redirect()->route('roles')->with('message', 'Роль успешно изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        if (!$role->users->count()) {
            $role->permissions()->detach();
            $role->delete();
            $message = 'Роль успешно удалена';
        } else {
            $message = 'Роль не может быть удалена, если за ней закреплены пользователи';
        }
        return redirect()->route('roles')->with('message', $message);

    }
}


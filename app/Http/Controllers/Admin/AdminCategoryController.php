<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminCategoryController extends Controller
{
    public function index()
    {
        return view('admin.category_menu');
    }

    public function editFormsForCreate()
    {
        $categories = Category::defaultOrder()->get();
        return view('admin.category_create_forms', ['categories' => $categories]);
    }

    public function create(Request $request)
    {
        $user = Auth::user();
        if ($user->canDo('create_category')) {
            $category = new Category();
            $category->name = $request->name;
            $category->image = 'фото';
            $category->parent_id = $request->parent_id;
            if ($category->save()) {
                return redirect()->route('admin_category')->with('message', 'Товар успешно добавлен');
            }
        } else {
            return redirect()->route('admin')
                ->with('message', 'Отсутствует доступ для осуществления данной операции');
        }
    }

    public function editFormsForDelete(CategoryService $service)
    {
        $categoriesTree = $service->getCategoriesTree();
        return view('admin.category_delete_forms', ['categoriesTree' => $categoriesTree]);
    }

    public function delete(Request $request)
    {
        $user = Auth::user();
        if ($user->canDo('delete_category')) {
            $category = Category::where('id', $request->id)->with('products')->first();
            $categories = Category::with('products')->descendantsAndSelf($request->id);
            $flag = 0;
            foreach ($categories as $node) {
                if ($node->products->all()) {
                    $flag = 1;
                }
            }
            if (!$flag) {
                $category->delete();
                return redirect()->route('admin_category')
                    ->with('message', 'Категория успешно удалена');
            } else {
                return redirect()->route('admin_category')
                    ->with('message',
                        'Категория не может быть удалена при наличии в ней или в дочерней категории товара'
                    );
            }
        } else {
            return redirect()->route('admin')
                ->with('message', 'Отсутствует доступ для осуществления данной операции');
        }
    }

    public function editFormsForUpdate(CategoryService $service)
    {
        $categoriesTree = $service->getCategoriesTree();
        return view('admin.category_update_forms', ['categoriesTree' => $categoriesTree]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        if ($user->canDo('update_category')) {
            Category::where('id', $request->id)->update(['name' => $request->new_name]);
            return redirect()->route('admin_category')
                ->with('message', 'Категория успешно переименована');

        } else {
            return redirect()->route('admin')
                ->with('message', 'Отсутствует доступ для осуществления данной операции');
        }
    }
}

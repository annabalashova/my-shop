<?php


namespace App\Http\Controllers;


use App\Services\OrderService;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    public function index($categoryId, OrderService $service)
    {
        $category = Category::where('id', $categoryId)->first();
        $ancestors = Category::defaultOrder()->ancestorsOf($categoryId);
        $products = Product::where([
            ['category_id', $categoryId],
            ['status','active']
        ])->get();

        $getCartProductsId = $service->getCartProductsId();
        return view('pages.products', [
            'products' => $products,
            'category' => $category,
            'ancestors' => $ancestors,
            'getCartProductsId' => $getCartProductsId
        ]);
    }

    public function show(Product $product, OrderService $service)
    {
        $getCartProductsId = $service->getCartProductsId();
        $ancestors = Category::defaultOrder()->ancestorsOf($product->category_id);
        return view('pages.product_card', [
            'product' => $product,
            'ancestors' => $ancestors,
            'getCartProductsId' =>$getCartProductsId
        ]);
    }
}

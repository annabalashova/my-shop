<?php


namespace App\Helpers;


class CartHelper
{
    public function getCartProducts()
    {
        $cartProducts = [];
        $idSession = session()->getId();
        if ($products = session()->get($idSession)) {
            foreach ($products as $items) {
                foreach ($items as $id => $product)
                    $cartProducts[] = $id;
            }
        }
        return $cartProducts;
    }
}

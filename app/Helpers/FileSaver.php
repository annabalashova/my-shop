<?php


namespace App\Helpers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileSaver
{
    public function upload(Request $request, $directory)
    {
        $source = $request->file('image');
        $extension = $source->extension();
        $path = $source->store('catalog/'.$directory.'/source', 'public');
        $path = Storage::disk('public')->path($path);
        $fileName = basename($path);
        $imagePath = 'catalog/'.$directory.'/image/';
        $image = Image::make($path)
            ->heighten(600)
            ->resizeCanvas(300, 600, 'center', false, 'eeeeee')
            ->encode($extension, 100);
        Storage::disk('public')->put($imagePath . $fileName, $image);
        $image->destroy();
        return $fileName;
    }

}

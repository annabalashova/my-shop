<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      /*  $categories = Category::withDepth()->having('depth', '<=', 1)->get();
        View:: share('categories', $categories);*/

        View::composer(
            '*', 'App\Composers\CartQuantityComposer'
        );

        View::composer(
            '*', 'App\Composers\CategoryComposer'
        );
    }


}

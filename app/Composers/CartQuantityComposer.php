<?php


namespace App\Composers;


use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CartQuantityComposer
{
    protected $quantity = 0;

    public function __construct()
    {
        $quantity = 0;

        if (Auth::id()) {
            $order = Order::where([
                ['user_id', Auth::id()],
                ['status', 'new']
            ])->first();
            if (!empty($order)) {
                foreach ($order->products as $product) {
                    $quantity += $product->pivot->quantity;
                }
            }
            $this->quantity = $quantity;
        } else {
            $id = session()->getId();

            if ($products = session()->get($id)) {
                foreach ($products as $items) {
                    foreach ($items as $product)
                        $quantity += $product['quantity'];
                }
                $this->quantity = $quantity;
            }
        }
    }

    public function compose(View $view)
    {

        $view->with('quantity', $this->quantity);
    }
}

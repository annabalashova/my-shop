<?php


namespace App\Composers;


use App\Models\Category;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CategoryComposer
{
    protected $categories;

    public function __construct()
    {
        $categories = Category::withDepth()->having('depth', '<=', 1)->get();
        $this->categories = $categories;
    }

    public function compose(View $view)
    {

        $view->with('categories', $this->categories);
    }
}

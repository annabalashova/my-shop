<?php

namespace App;

use App\Models\Order;
use App\Models\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        $roles = $this->roles()->pluck('name')->toArray();

        return in_array($role, $roles);
    }

    public function canDo($permission)
    {
        $permissions = [];
        foreach ($this->roles as $role) {
            $permissions = array_merge($permissions, array_column($role->permissions->toArray(), 'name'));
        }
        $permissions = array_unique($permissions);

        return in_array($permission, $permissions);
    }
}

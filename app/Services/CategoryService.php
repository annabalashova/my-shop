<?php


namespace App\Services;


use App\Models\Category;

class CategoryService
{
    public function getCategoriesTree()
    {
        $nodes = Category::get()->toTree();
        $categoriesTree = [];
        $i = 0;
        $traverse = function ($categories, $prefix = ' ') use (&$traverse, &$categoriesTree, $i) {
            foreach ($categories as $category) {
                $i += 1;
                $categoriesTree[$category->id] = $prefix . $i . ' ' . $category->name;
                $traverse($category->children, $prefix . '--');
            }
        };

        $traverse($nodes);
        return $categoriesTree;
    }
}

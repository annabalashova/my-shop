<?php


namespace App\Services;


use App\Models\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    public function getCartProductsId()
    {
        $cartProductsId = [];
        $idSession = session()->getId();
        if ($cartProducts = session()->get($idSession)) {
            foreach ($cartProducts as $items) {
                foreach ($items as $id => $product)
                    $cartProductsId[] = $id;
            }
        }
        return $cartProductsId;
    }

    protected function getOrder()
    {
        $order = Order::where([
            ['user_id', Auth::id()],
            ['status', 'new']
        ])->first();
        return $order;
    }

    public function setOrder()
    {
        $order = $this->getOrder();
        if (empty($order)) {
            $order = new Order();
            $order->user_id = Auth::id();
            $order->date = Carbon::now();
            $order->status = 'new';
            $order->save();
        }
        return $order;
    }
}
